

## Reference API Proxy 

Instructions for fei-devops-jltraining

[TOC levels=1-6]: # "# Table of Contents"

# Table of Contents
- [Reference API Proxy](#reference-api-proxy)
- [Description](#description)
- [Purpose](#purpose)
- [Pre-Requisites](#pre-requisites)
- [External configuration](#external-configuration)
    - [Access token generation](#access-token-generation)
        - [Acurl and get_token](#acurl-and-get_token)
    - [Maven cicd settings xml config](#maven-cicd-settings-xml-config)
- [Usage Instructions](#usage-instructions)
- [Mandatory practices for API Proxy Development](#Mandatory-practices-for-API-Proxy-Development)
    - [JSON Request Validation](#JSON-Request-Validation)
    - [Steps to implement JSON Validation](#Steps-to-implement-JSON-Validation)
- [Lifecycle for API proxy deployment](#lifecycle-for-api-proxy-deployment)
    - [Lint Proxy](#lint-proxy)
    - [Unit Tests](#unit-tests)
    - [Bundle Package](#bundle-package)
    - [Upload Caches](#upload-caches)
    - [Upload Target Servers](#upload-target-servers)
    - [Upload KVM's](#upload-kvms)
    - [Deploy Proxy](#deploy-proxy)
    - [Upload Products](#upload-products)
    - [Upload Developers](#upload-developers)
    - [Upload Apps](#upload-apps)
    - [Export Keys](#export-keys)
    - [Integration Tests](#integration-tests)
    - [Upload to Artifactory](#upload-to-artifactory)



## Description

While deploying an API proxy to any targeted Apigee Edge env, it must go through several CI 
phases such as Linting, Code Coverage, Unit testing, Build and packaging, configuring environment
level dependencies (i.e. KVM's, Caches, Target Servers), Configuring org level entities (i.e. API products,
Developers, Apps) and integration testing. Also, artifacts must be deployed to the remote locations (i.e. Nexus).

    
## Purpose

The DevOps engineer should be able to execute all the above mentioned CI phases by manually running
the steps.

## Pre-Requisites 
- JDK 8
- Apache Maven
- Linux/WSL
- Settings.xml file in local 
- SAML Tokens (Access & Refresh)

## External configuration

The external configuration references required for token generation and an external settings.xml

### Access token generation

In order to generate the access token, follow the below link:

https://bitbucket.org/feiapi/apigee-devops-access_token/src/master/

Go [here](https://ferguson.login.apigee.com/passcode) to generate temporary auth codes.

#### Acurl and get_token

https://docs.apigee.com/api-platform/system-administration/auth-tools

Install `acurl` and `get_token`

```bash
# download and install acurl and get_token
curl https://login.apigee.com/resources/scripts/sso-cli/ssocli-bundle.zip -o "ssocli-bundle.zip"
unzip -n ssocli-bundle.zip
sudo ./install -b /usr/local/bin

# check installation
acurl -h
get_token -h
```

Use acurl to fetch refresh and access tokens:
```bash
# configure acurl
export SSO_LOGIN_URL=https://ferguson.login.apigee.com
export CLIENT_AUTH=ZWRnZWNsaTplZGdlY2xpc2VjcmV0

# run acurl; passcode is fetched from https://ferguson.login.apigee.com/passcode
acurl https://api.enterprise.apigee.com/v1/organization/ferguson-api -p $PASSCODE
```

### Maven cicd settings xml config

A template for the cicd settings xml (which must be configured into Jenkins as a global config file)
is posted here:

https://bitbucket.org/feiapi/apigee-devops-mavensettingsfile/src/master/


## Usage Instructions

This Api Proxy project will be used to deploy fei-devops-jltraining to apigee edge. 

How to clone the apigee-devops-jltraining and change the directory 

```bash
# clone apigee-devops-jltraining repository
git clone https://bitbucket.org/feiapi/apigee-devops-jltraining.git

# change directory into the repository
cd apigee-devops-jltraining
```

## Mandatory practices for API Proxy Development

If JSON Request (Body) Validation is not needed, then remove this code from the PreFlow: 

```bash
 <Step>
    <Name>JS-validationRequest</Name>
    <Condition>((request.verb = "POST") or (request.verb = "PUT"))</Condition>
 </Step>             
```  

### JSON Request Validation 

Apigee does not have a built-in policy to validate requests against an OAS, like it does for XSDs. Illegal elements 
can be sent as part of the request body while making a POST and PUT request. Illegal elements are key value pairs which
are not defined in the swagger document but sent as part of the request body. This needs to be validated against OAS.


### Steps to implement JSON Validation 

Step 1: Verify an “oas.js” file in the directory path listed below, then create the variable to hold the ENTIRE Swagger file (JSON format).  

Directory path as:

```bash
apigee-devops-jltraining/edge/apiproxy/resources/jsc/oas.js
```

It should look like this: 

```bash
var oas = {
            "swagger": "2.0",
            "info": {
                "description": "This is a sample server Petstore server.  You can find out more about Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, you can use the api key `special-key` to test the authorization filters.",
                "version": "1.0.5",
                "title": "Swagger Petstore",
                "termsOfService": "http://swagger.io/terms/",
                "contact": {
                    "email": "apiteam@swagger.io"
                },
                -----------
                -----------
                -----------
            "externalDocs": {
                       "description": "Find out more about Swagger",
                       "url": "http://swagger.io"
            }
};
```

Step 2: Make sure your swagger file has POSTs/PUTs/PATCHES/DELETES in the proper format as follows:
```bash
"parameters" : [ {
          "in" : "body",
          "name" : "body",
          "required" : true,
          "schema" : {
            "$ref" : "#/definitions/YOUR_DEFINITION_NAME_HERE"
          }
        } ]       
``` 

Step 3: Make sure you only use $ref in the schema section as follows:
```bash
        "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "Pet object that needs to be added to the store",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/Pet"
                        }
                    }
                ],
```

This is not an acceptable format of how to define the parameter section using $ref:
```bash
post": {
        "description": "This operation post details like app name,api products,expiry time\nand attributes\n",
        "parameters": {
          "-$ref": "#/definitions/requestBody"
        },
        "responses": {
          "201": {
            "description": "Successfully created" }
```
    
Step 4: The following code in validateRequest.js file at line #27 will restrict additional elements from appearing
within the request body that are not defined in the Swagger.  Ensure it is set to “true”.  This is the default behavior
and should never be set to “false” or commented out. 

```bash
     var result = tv4.validateMultiple(body, schema, "" ,true);    
```

Step 5: If you want to allow elements that are not defined in the swagger and are in addition to the required elements
of the request body, then set additionalProperties to "true" in the swagger:
 
```bash
  requestBody:
          content:
            application/x-www-form-urlencoded:
              schema:
                type: object
                additionalProperties: true    # this line is optional  

```
Note: Swagger properties will take precedence over validateRequest.js file.  Again, no change is required to
validateRequest.js file at line #27.  Do not set it to "false" or comment out.

## Lifecycle for API proxy deployment 

To make local invocation easier, load the app token from an environment variable

Example of how to export a local environment variable:
```bash
export APIGEE_APITOKEN=abc123
```

### Lint Proxy

```
# invoke apigeelint directly
HTML View
apigeelint -s edge/apiproxy -f html.js

OR, Table View
apigeelint -s edge/apiproxy -f table.js

# invoke linting through maven; run from /edge directory in project root
cd edge
mvn test -Pproxy-linting             
```
 
### Unit Tests
```
# run from /edge directory in project root
mvn test -Pproxy-unit-test
```
 
### Bundle Package

run this from `edge` directory:
```bash
mvn package \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth
```
 
### Upload Caches

```bash
mvn apigee-config:caches \
    -Papigee  \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
### Upload Target Servers

```bash
mvn apigee-config:targetservers \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
### Upload KVM's

```bash
mvn apigee-config:keyvaluemaps \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
### Deploy Proxy

```bash
mvn apigee-enterprise:deploy \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
### Upload Products

```bash
mvn apigee-config:apiproducts \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=http \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
### Upload Developers

```bash
mvn apigee-config:developers \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
### Upload Apps

```bash
mvn apigee-config:apps \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
### Export Keys

```bash
mvn apigee-config:exportAppKeys \
    -Papigee \
    -Denv=dev \
    -Dorg=ferguson-api \
    -DvhostProtocol=https \
    -DvhostDomainName=devapi2.ferguson.com \
    -DvhostDomainPort=443 \
    -DvhostEdgeName=fei-secure \
    -Dapigee.config.dir=target/resources/edge \
    -Dapigee.config.options=create \
    -Dapigee.config.exportDir=target/test/integration \
    -Dapigee.api.port=8080 \
    -Dapigee.api.host=api.enterprise.apigee.com \
    -Dapigee.api.protocol=https \
    -Dtokenurl=https://ferguson.login.apigee.com/oauth/token \
    -Dauthtype=oauth \
    -Dbearer= ************* \
    -Drefresh= ***************
```
 
 
### Integration Tests

Invoke Maven and get the required configurations 

```bash
mvn clean test \
    -Dorg=ferguson-api \
    -Denv=dev \
    -Dkey=******** \
    -Dsecret=********* \
```
Execute node command to run integration test
```bash
node ./node_modules/cucumber/bin/cucumber-js target/test/integration/features --format json:target/reports.json
```

### Upload to Artifactory 

```bash
Disable for developers. Only CI Server can deploy to the remote location. 
```

